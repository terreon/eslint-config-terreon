# ESLint Config Terreon

[![NPM version](https://img.shields.io/npm/v/eslint-config-terreon.svg?style=flat)](https://npmjs.org/package/eslint-config-terreon)
[![NPM downloads](https://img.shields.io/npm/dm/eslint-config-terreon.svg?style=flat)](https://npmjs.org/package/eslint-config-terreon)

#### An ESLint [Shareable Config](http://eslint.org/docs/developer-guide/shareable-configs) for [JavaScript Standard Style](http://standardjs.com) with some changes:
>
> - **4 spaces** – for indentation 
> - **No space after function name** – `function name(arg) { ... }`
> - **No spaces required inside comments** – `//console.log('foo')`

## Installation

```sh
npm install eslint-config-standard
```

## Usage

Shareable configs are designed to work with the `extends` feature of `.eslintrc` files.
You can learn more about
[Shareable Configs](http://eslint.org/docs/developer-guide/shareable-configs) on the
official ESLint website.

To use the JavaScript Standard Style shareable config, first run this:

```sh
npm install --save-dev eslint-config-terreon
```

Then, add this to your .eslintrc file:

```
{
  "extends": "terreon"
}
```

*Note: We omitted the `eslint-config-` prefix since it is automatically assumed by ESLint.*

You can override settings from the shareable config by adding them directly into your
`.eslintrc` file.

## License

Apache 2.0
